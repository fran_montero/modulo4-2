const inputTipo = document.getElementById('tipoHabitacion');
const inputSpa = document.getElementById('spaCheckbox');
const inputOcupacion = document.getElementById('ocupacionHabitacion');
const inputNoches = document.getElementById('numNoches');
const inputParking = document.getElementById('numNochesParking');

//Tipo
const tarifaStandard = 100;
const tarifaJunior  = 120;
const tarifaSuite = 150;
//Spa
const tarifaSpa = 20;
//Ocupación
const tarifaIndividual = -0.25;
const tarifaDoble = 0;
const tarifaTriple = 0.25;
//Parking
const tarifaParking = 10;

let precioTipo = 0;
let precioSpa = 0;
let porcentajeOcupacion = 0;
let numNoches = 0;
let numParking = 0;

let precioFinal = 0;

//Función que se lanza al modificar el selector de tipo de habitación
function setTipo() {
    let optionSelect = inputTipo.options[inputTipo.selectedIndex].value;
    switch (optionSelect) {
        case 'standard':
            precioTipo = tarifaStandard;
            break;
        case 'junior':
            precioTipo = tarifaJunior;
            break;
        case 'suite':
            precioTipo = tarifaSuite;    
            break;
        default:
            alert("Seleccione el tipo de habitación");
            break;
    }
    //Actualizamos el cálculo final
    calcular();
}

// Función que se lanza cuando se modifica el checkbox del uso del SPA
function setSpa() {
    if (inputSpa.checked) {
        precioSpa = tarifaSpa;
    } else {
        precioSpa = 0;
    }
    //Actualizamos el cálculo final
    calcular();
}

function setOcupacion() {
    let optionSelect = inputOcupacion.options[inputOcupacion.selectedIndex].value;
    switch (optionSelect) {
        case 'individual':
            porcentajeOcupacion = tarifaIndividual;
            break;
        case 'doble':
            porcentajeOcupacion = tarifaDoble;
            break;
        case 'triple':
            porcentajeOcupacion = tarifaTriple;    
            break;
        default:
            alert("Seleccione el tipo de ocupación");
            break;
    }
    //Actualizamos el cálculo final
    calcular();  
}

// Función que se lanza al modificar el contenido del input de número de noches
inputNoches.addEventListener('input', function() {
    numNoches = inputNoches.value;
    //Actualizamos el cálculo final
    calcular();  
});

// Función que se lanza al modificar el contenido del input de número de noches de parking
inputParking.addEventListener('input', function() {
    numParking = inputParking.value;
    //Actualizamos el cálculo final
    calcular();  
});

//Función que calcula y actualiza el precio final
function calcular() {
    // Calculamos el precio de la habitación con el SPA y se le aplica los modificadores de categoría según el tipo de ocupación
    let precioTipoSpa = precioTipo + precioSpa;
    let precioPorcentual = precioTipoSpa + (precioTipoSpa * porcentajeOcupacion);

    // Calculamos el precio final    
    precioFinal = (precioPorcentual * numNoches) + (tarifaParking * numParking);

    // Actualizamos resultado 
    document.getElementById('precioFinal').innerHTML = precioFinal + " €";
    
}