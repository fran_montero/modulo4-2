let resultadoOperaciones = 0;
let operacionAnterior = "";
let numInput =  document.getElementById('numberOne');

function suma() {
    ejecutarOperacion('suma');
}

function resta() {
    ejecutarOperacion('resta');
}

function multiplica() {
    ejecutarOperacion('multiplica');
}

function divide() {
    ejecutarOperacion('divide');
}

function calcula() {
    ejecutarOperacion('calcula');
}

function ejecutarOperacion(operacion) {
    let num1 = numInput.value;
    num1 = parseInt(num1);
    numInput.value = "";
    numInput.focus();
    
    if((num1 == "" || isNaN(num1) ) && operacion != "calcula") {
        alert("Por favor, rellena la caja texto con un número");
    } else if(operacion == "calcula") {
        resultadoOperaciones = calculaResultado(num1, operacionAnterior);
        document.getElementById('resultadoCalculadora').innerHTML = resultadoOperaciones;
        document.getElementById('divResultado').style.display = "inline";

        resultadoOperaciones = 0;
        operacionAnterior = "";
    
    } else {
        if (operacionAnterior == "") {
            resultadoOperaciones = num1;
        } else {
            resultadoOperaciones = calculaResultado(num1, operacionAnterior)
        }
        operacionAnterior = operacion;
        } 

}

function calculaResultado (num, operacion) {
    switch (operacion) {
        case 'suma':
            resultadoOperaciones += num;
            break;
        case 'resta':
            resultadoOperaciones -= num;
            break;
        case 'multiplica':
            resultadoOperaciones *= num;
            break;
        case 'divide':
            resultadoOperaciones /= num;
            break;
    }
    return resultadoOperaciones;
}
